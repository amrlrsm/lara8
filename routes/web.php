<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('trippy');
    // return view('landing');
});



Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])
->group(function () {

    // Route::get('/homepage', function () {
    //     return view('layouts.homepage');
    // })->name('homepage');

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/homepage', [BookController::class,'home'])
    ->name('homepage');

    Route::get('/book', [BookController::class,'index'])
    ->name('book');

    Route::get('/profile', [BookController::class,'profile'])
    ->name('profile');



});
